import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import { mapAlbum, mapTrack } from './DataMapping';

class AlbumDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      album: {},
      tracks: [],
    }
  }

  componentDidMount() {
    this.getAlbum();
    this.getTracks();
  }

  getAlbum() {
    console.log('getAlbum:', this);
    const url = 
      `http://localhost:5000/api/library/albums/${this.props.match.params.id}`;
    fetch(url)
      .then(res => res.json())
      .then(album => this.setState((prevState, props) => {
        return { album: mapAlbum(album)};
      }));
      //TODO: get cover art url
  }

  getTracks() {
    const url = 
      `http://localhost:5000/api/library/tracks?sort=tracknum&album_id=${this.props.match.params.id}`;
    fetch(url)
      .then(res => res.json())
      .then(tracks => this.setState((prevState, props) => {
        return { tracks: tracks.map(mapTrack)};
      }));
  }

  render() {
    let imgUrl = "http://euterpe3:9000/music/" + this.state.album.coverid + "/cover.jpg";
    return (
      <div className="album-detail">
        <img className="cover-art"
            alt={this.state.album.title} 
            src={imgUrl}/>
        <span>
          <span className="title">{this.state.album.name} </span>
          <span className="controls">
            <button type="button" 
                onClick={() => this.props.handleAdd('album', this.state.album.id)}
                title="add to playlist">
              +
            </button>
            <button type="button" 
                onClick={() => this.props.handlePlay('album', this.state.album.id)}
                title="play">></button>
          </span>
        </span>
        <div className="artist">
          <NavLink to={"/albums?artist_id=" + this.state.album.artist_id}>
            {this.state.album.artist}
          </NavLink>
        </div>
        <div className="year">
          <NavLink to={"/albums?year=" + this.state.album.year}>
            {this.state.album.year}
          </NavLink>
        </div>
        <div className="tracks">
          {this.state.tracks.map((track) => {
            return (
              <div key={track.id} className="track">
                <div className="title">{track.title}</div>
                <div className="artist">
                  <NavLink to={"/albums?artist_id=" + track.artist_id}>
                    {track.artist}
                  </NavLink>
                </div>
                <div className="controls">
                  <button type="button" 
                      onClick={() => this.props.handleAdd('track', track.id)}
                      title="add to playlist">
                    +
                  </button>
                  <button type="button" 
                      onClick={() => this.props.handlePlay('track', track.id)}
                      title="play">></button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default AlbumDetail;
