import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { mapAlbum } from './DataMapping';

class Albums extends Component {
  constructor(props) {
    super(props);
    this.state = {
      albums: [],
    };
  }

  componentDidMount() {
    this.getAlbums();
  }

  getAlbums() {
    const url = 
      `http://localhost:5000/api/library/albums${this.props.location.search}`;
    fetch(url)
      .then(res => res.json())
      .then(albums => this.setState((prevState, props) => {
        return { albums: albums.map(mapAlbum)};
      }));
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.location.search !== prevProps.location.search) {
      this.getAlbums();
    }
  }

  trunc(s) {
    if(s.length > 50) {
      return s.substring(0, 47) + '...';
    }
    return s;
  }
  /*
  */ 
  render() {
    return (
      <ol className="albums">
        {this.state.albums.map(album => {
          return (
            <div className="album" key={album.id}> 
              <img className="cover-art"
                  alt={album.title} 
                  src={"http://euterpe3:9000/music/" + album.coverid + "/cover.jpg"}/>
              <div className="title">
                <NavLink to={"/albums/" + album.id}>
                  {this.trunc(album.name)}
                </NavLink>
              </div>
              <div className="artist">
                <NavLink to={"/albums?artist_id=" + album.artist_id}>
                  {album.artist}
                </NavLink>
              </div>
              <div className="controls">
                <button type="button" 
                    onClick={() => this.props.handleAdd('album', album.id)}
                    title="add to playlist">
                  +
                </button>
                <button type="button" 
                    onClick={() => this.props.handlePlay('album', album.id)}
                    title="play">></button>
              </div>
            </div>
          );
        })}
      </ol>
    );
  } 
}

export default Albums;

