import React, { Component } from 'react';
import {
  Route,
  NavLink,
  Redirect,
  Switch,
  HashRouter
} from 'react-router-dom';
import './App.css';
import Artists from './Artists';
import Albums from './Albums';
import AlbumDetail from './AlbumDetail';
import Years from './Years';
import Players from './Players';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPlayerId: null,
    };
  }

  handlePlayerSelect(player_id) {
    console.log('handlePlayerSelect:', this.state.selectedPlayerId, '->', player_id);
    this.setState((prevState, props) => {
      return {selectedPlayerId: player_id};
    });
  }

  handleAdd(type, id) {
    console.log('handleAdd:', type, id, 'to', this.state.selectedPlayerId);
    fetch(`http://localhost:5000/api/players/${this.state.selectedPlayerId}/playlist/tracks`, {
        method: 'POST',
        headers: {'Content-type': 'application/json'},
        body: JSON.stringify({ type: type, id: id }),
      }
    );
  }

  handlePlay(type, id) {
    console.log('handleAdd:', type, id, 'to', this.state.selectedPlayerId);
    fetch(`http://localhost:5000/api/players/${this.state.selectedPlayerId}/playlist/tracks`, {
        method: 'PUT',
        headers: {'Content-type': 'application/json'},
        body: JSON.stringify({ type: type, id: id }),
      }
    );
  }

  render() {
    let playlistHandlers = {
      handleAdd: ((type, id) => this.handleAdd(type, id)),
      handlePlay: ((type, id) => this.handlePlay(type, id)),
    };
    return (
        <div className="App">
          <header>
          </header>
          <HashRouter>
            <div>
              <div className="players">
                <Players handlePlayerSelect={(id) => this.handlePlayerSelect(id)}/>
              </div>
              <div className="library">
                <div className="nav">
                  <NavLink to="/">New Music</NavLink>
                  <NavLink style={{padding: "20px"}} to="/artists">Artists</NavLink>
                  <NavLink to="/albums">Albums</NavLink>
                  <NavLink to="/years">Years</NavLink>
                </div>
                <div className="content">
                  <Switch>
                    <Route exact path="/" render={
                        () => <Redirect to="/albums?sort=new"/> }/>
                    <Route path="/albums/:id" 
                        render={(routeProps) => (
                          <AlbumDetail 
                            {...routeProps} 
                            {...playlistHandlers}/>
                        )}/>
                    <Route path="/albums" 
                        render={(routeProps) => (
                          <Albums
                            {...routeProps} 
                            {...playlistHandlers}/>
                        )}/>
                    <Route path="/artists"
                        render={(routeProps) => (
                          <Artists
                            {...routeProps} 
                            {...playlistHandlers}/>
                        )}/>
                    <Route path="/years" component={Years}/>
                  </Switch>
                </div>
              </div>
            </div>
          </HashRouter>
        </div>
    );
  }
}

export default App;
