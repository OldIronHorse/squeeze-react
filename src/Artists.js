import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Artists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artists: [],
    };
  }

  componentDidMount() {
    fetch('http://localhost:5000/api/library/artists')
      .then(res => res.json())
      .then(artists => this.setState((prevState, props) => {
        return { artists: artists.map(this.mapArtist)};
      }));
  }
   
  mapArtist(artist) {
    return {
      id: artist.id,
      name: artist.name,
    };
  }

  render() {
    const artists = this.state.artists.map(artist => {
      let url = `/albums?artist_id=${artist.id}`
      return (
        <div className="artist" key={artist.id}>
          <div className="name">
            <NavLink to={url}>{artist.name}</NavLink>
          </div>
          <div className="controls">
            <button type="button" 
                onClick={() => this.props.handleAdd('artist', artist.id)}
                title="add to playlist">
              +
            </button>
            <button type="button" 
                onClick={() => this.props.handlePlay('artist', artist.id)}
                title="play">></button>
          </div>
        </div>
      );}
    );
    return <div className="artists">{artists} </div>;
  } 
}

export default Artists;
