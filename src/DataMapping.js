export function mapTrack(track) {
  return {
    id: track.id,
    title: track.title,
    album: track.album,
    album_id: track.album_id,
    artist: track.artist,
    artist_id: track.artist_id,
    index: track.index,
    coverid: track.coverid,
  }
}

export function mapAlbum(album) {
  return {
    id: album.id,
    name: album.name,
    artist: album.artist,
    artist_id: album.artist_id,
    title: album.title,
    year: album.year,
    compilation: album.compilation,
    coverid: album.artwork_track_id,
  };
}

