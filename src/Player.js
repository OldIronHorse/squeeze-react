import React, { Component } from 'react';

class Player extends Component {

  handleMode(cmd) {
    //TODO: other commands...
    console.log('handleCmd:', this.props.player.id, cmd);
    fetch(`http://localhost:5000/api/players/${this.props.player.id}/mode`, {
      method: 'PUT',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({ mode: cmd }),
    });
  }   

  handleTrack(delta) {
    console.log('handleTrack:', this.props.player.id, delta);
    fetch(`http://localhost:5000/api/players/${this.props.player.id}/playlist_current_index`, {
      method: 'PUT',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({ delta: delta }),
    });
  }

  handleVolume(delta) {
    console.log('handleTrack:', this.props.player.id, delta);
    fetch(`http://localhost:5000/api/players/${this.props.player.id}/volume`, {
      method: 'PUT',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({ delta: delta }),
    });
  } 

render() {
    console.log("Player.render:", this.props);
    return (
      <div className="player">
        <div className="playback controls">
          <button type="button" 
              title="previous track" 
              onClick={() => this.handleTrack(-1)}>
            |&lt;
          </button>
          {this.props.player.mode === 'play' ?
            <button type="button" 
                title="pause"
                onClick={() => this.handleMode('pause')}>
              ||
            </button> :
            <button type="button" 
                title="play" 
                onClick={() => this.handleMode('play')}>
              &gt;
            </button>}
          <button type="button" 
              title="next track" 
              onClick={() => this.handleTrack(+1)}>
            &gt;|
          </button>
        </div>
        <div className="volume controls">
          <button type="button" 
            title="quieter" 
            onClick={() => this.handleVolume(-2)}>
          -
        </button>
          <span>{this.props.player.volume}</span>
          <button type="button" 
            title="louder" 
            onClick={() => this.handleVolume(2)}>
          +
        </button>
        </div>
      </div>
    );
  }
}

export default Player;
