import React, { Component } from 'react';

import connect from 'mqtt';

import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import './react-tabs.css';

import { mapTrack } from './DataMapping';
import Player from './Player';
import Playlist from './Playlist';
import './Players.css';

class Players extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: [],
    };
  }

  componentDidMount() {
    fetch('http://localhost:5000/api/players')
      .then(res => res.json())
      .then(players => {
        for(let i = 0; i < players.length; i++) {
          this.getPlayer(i, players[i].id);
        
      }});
    let mqttClient = connect('mqtt://euterpe3:1884');
    mqttClient.on('connect', () => {
      console.log("MQTT connected.");
      mqttClient.subscribe('squeezebox/players/+');
    });
    mqttClient.on('message', (topic, msg) => {
      console.log('MQTT message:', topic, JSON.parse(msg.toString()));
      let player = this.mapPlayer(JSON.parse(msg.toString()));
      this.setState((prevState, props) => {
        let players = prevState.players.slice();
        for(let i = 0; i < players.length; i++) {
          if(players[i].id === player.id) {
            players[i] = player;
            return { players: players };
          }
        }
        players.push(player);
        return { players: players };
      });
    });
  }

  getPlayer(index, id) {
    console.log('getPlayer', index, id);
    fetch(`http://localhost:5000/api/players/${id}`)
      .then(res => res.json())
      .then(player => this.setState((prevState, props) => {
        let players = prevState.players.slice();
        players[index] = this.mapPlayer(player['player']);
        return { players: players };
      }));
  }

  mapPlayer(player) {
    console.log('mapPlayer:', player);
    return {
      id: player.id,
      mode: player.mode,
      name: player.name,
      // TODO: map playlist
      playlist: player.playlist.map(mapTrack),
      volume: player.volume,
      playlist_index: player.playlist_cur_index,
    };
  }

  handleTabSelect(index, prevIndex, event) {
    console.log('handleTabSelect:', this.state.players[index].id);
    this.props.handlePlayerSelect(this.state.players[index].id);
  }

  render() {
    console.log('Players.render:', this.state);
    return (
      <Tabs defaultIndex={1} 
          onSelect={(i, pi, e) => (
            this.handleTabSelect(i, pi, e)
          )}>
        <TabList>
          {this.state.players.map(player => {
            return <Tab key={player.id}>{player.name}</Tab>
          })}
        </TabList>
        {this.state.players.map(player => {
          return (
            <TabPanel key={player.id}>
              <Player 
                  player={player}/>
              <Playlist
                  player={player}/>
            </TabPanel>
          );
        })}
      </Tabs>
    );
  }

}

export default Players;
