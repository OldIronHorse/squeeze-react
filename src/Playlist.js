import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './Players.css';

class Playlist extends Component {

  handleRemove(index) {
    fetch(`http://localhost:5000/api/players/${this.props.player.id}/playlist/tracks/${index}`,{
      method: 'DELETE',
    });
  } 

  render() {
    return (
      <div className="tracks">
        {this.props.player.playlist.map((track) => {
          return (
            <div key={track.index} 
                className={ track.index === this.props.player.playlist_index ?
                "track playing" : 
                track.index < this.props.player.playlist_index ?
                  "track played" :
                  "track" }>
              <div className="title">
                {track.title}
              </div>
              <div className="artist">
                <NavLink to={"/albums?artist_id=" + track.artist_id}>
                  {track.artist}
                </NavLink>
              </div>
              <div className="album">
                <NavLink to={"/albums/" + track.album_id}>
                  {track.album}
                </NavLink>
              </div>
              <div className="title">
                {this.props.player.title}
              </div>
              <div className="controls">
                <button type="button"
                    title="remove from playlist"
                    onClick={() => this.handleRemove(track.index)}>
                  x
                </button>
              </div>
            </div>
          );
        })};
      </div>
    );
  }
}

export default Playlist;
