import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Years extends Component {
  constructor(props) {
    super(props);
    this.state = {
      years: [],
    };
    this.url = 'http://localhost:5000/api/library/years';
  }

  componentDidMount() {
    fetch(this.url)
      .then(res => res.json())
      .then(years => this.setState((prevState, props) => {
        return { years: years };
      }));
  }
   
  render() {
    return (
      <div className="years">
        {this.state.years.map(year => {
          const url = `/albums?year=${year}`;
          return (
            <div className="year" key={year}>
              <NavLink to={url}>{year}</NavLink>
            </div>
          );
        })}
      </div>
    );
  } 
}

export default Years;

